/*
 * Copyright (c) 2010-2024. Institut Pasteur.
 *
 * This file is part of Icy.
 * Icy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Icy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Icy. If not, see <https://www.gnu.org/licenses/>.
 */

package plugins.tmusset.script.engine;

import com.oracle.truffle.js.scriptengine.GraalJSScriptEngine;
import org.bioimageanalysis.icy.extension.plugin.annotation_.IcyPluginIcon;
import org.bioimageanalysis.icy.extension.plugin.annotation_.IcyPluginName;
import org.bioimageanalysis.icy.system.IcyExceptionHandler;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Engine;
import org.graalvm.polyglot.HostAccess;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import plugins.tmusset.script.core.AbstractEngine;
import plugins.tmusset.script.core.EngineException;

import javax.script.ScriptException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author Thomas MUSSET
 */
@IcyPluginName("Script Manager - GraalVM")
//@IcyPluginDescription(shortDesc = "")
@IcyPluginIcon(path = "/plugins/tmusset/script/engine/graaljs/icon/JavaScript_logo.svg")
public final class GraalVMJSEngine extends AbstractEngine<GraalJSScriptEngine> {
    private static AbstractEngine<GraalJSScriptEngine> instance = null;

    public static AbstractEngine<GraalJSScriptEngine> getInstance() {
        if (instance == null)
            instance = new GraalVMJSEngine();

        return instance;
    }

    /**
     * GraalVMJSEngine constructor.
     */
    private GraalVMJSEngine() {
        super("GraalVM", "JavaScript", new String[]{"js"});
    }

    /**
     * {@inheritDoc}
     *
     * @param name {@inheritDoc}
     * @return {@inheritDoc}
     */
    @Override
    protected Interpreter<GraalJSScriptEngine> createInterpreterInstance(final @NotNull String name) throws EngineException {
        try {
            final Engine.Builder eb = Engine.newBuilder().option("engine.WarnInterpreterOnly", "false");
            final Context.Builder cb = Context.newBuilder("js").allowHostAccess(HostAccess.ALL).allowHostClassLookup(s -> true);
            return new GraalVMJSInterpreter(GraalJSScriptEngine.create(eb.build(), cb), name);
        }
        catch (final Throwable t) {
            throw new EngineException(this, t);
        }
    }

    @SuppressWarnings("unused")
    // TODO: 02/05/2023 To be removed
    private void test() {
        final InputStream stream = this.getClass().getResourceAsStream("/plugins/tmusset/script/sample/test.js");

        if (stream == null) {
            System.err.println("test file not found in resources");
            return;
        }

        final InputStreamReader reader = new InputStreamReader(stream);

        /*try (
                final Engine engine = Engine.newBuilder()
                        .option("engine.WarnInterpreterOnly", "false")
                        .build()
        ) {
            try (
                    final Context ctx = Context
                            .newBuilder("js")
                            .engine(engine)
                            .allowHostAccess(HostAccess.ALL)
                            .allowHostClassLookup(s -> true)
                            .build()
            ) {
                ctx.eval("js", "console.log('Greetings!');");

                final Source source = Source.newBuilder("js", url).build();
                ctx.eval(source);

                ctx.eval("js", "console.log(function1('Thomas'))");
            }
            catch (final IOException e) {
                e.printStackTrace();
            }
        }*/

        try (
                final Engine engine = Engine.newBuilder()
                        .option("engine.WarnInterpreterOnly", "false")
                        .build()
        ) {
            final Context.Builder ctx = Context
                    .newBuilder("js")
                    .allowHostAccess(HostAccess.ALL)
                    .allowHostClassLookup(s -> true);

            try (final GraalJSScriptEngine scriptEngine = GraalJSScriptEngine.create(engine, ctx)) {
                //scriptEngine.put("javaObj", new Object());
                //scriptEngine.eval("(javaObj instanceof Java.type('java.lang.Object'));");

                scriptEngine.eval(reader);
                final Object result = scriptEngine.invokeFunction("getSquare", 5);

                System.out.println(result);
            }
            catch (final ScriptException | NoSuchMethodException e) {
                IcyExceptionHandler.showErrorMessage(e, true);
            }
        }
    }

    public final class GraalVMJSInterpreter extends AbstractInterpreter {
        GraalVMJSInterpreter(final GraalJSScriptEngine context, final String name) {
            super(GraalVMJSEngine.getInstance(), context, name);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void destroy() {
            context.close();
        }

        /**
         * {@inheritDoc}
         *
         * @param stream {@inheritDoc}
         * @param name   {@inheritDoc}
         */
        @Override
        public void loadFileAsStream(final @NotNull InputStream stream, final @NotNull String name) throws EngineException {
            final InputStreamReader reader = new InputStreamReader(stream);

            try {
                context.eval(reader);
            }
            catch (final Throwable t) {
                throw new EngineException(this, t);
            }
        }

        /**
         * {@inheritDoc}
         *
         * @param script {@inheritDoc}
         * @return {@inheritDoc}
         */
        @Override
        public @Nullable Object runScript(final @NotNull String script) throws EngineException {
            try {
                return context.eval(script);
            }
            catch (final Throwable t) {
                throw new EngineException(this, t);
            }
        }

        /**
         * {@inheritDoc}
         *
         * @param name  {@inheritDoc}
         * @param value {@inheritDoc}
         */
        @Override
        public void putVariable(final @NotNull String name, final @NotNull Object value) throws EngineException {
            try {
                context.put(name, value);
            }
            catch (final Throwable t) {
                throw new EngineException(this, t);
            }
        }

        /**
         * {@inheritDoc}
         *
         * @param name {@inheritDoc}
         * @return {@inheritDoc}
         */
        @Override
        public @Nullable Object getVariable(final @NotNull String name) throws EngineException {
            try {
                return context.get(name);
            }
            catch (final Throwable t) {
                throw new EngineException(this, t);
            }
        }

        /**
         * {@inheritDoc}
         *
         * @param name {@inheritDoc}
         * @param args {@inheritDoc}
         * @return {@inheritDoc}
         */
        @Override
        public @Nullable Object callFunction(final @NotNull String name, final @NotNull Object... args) throws EngineException {
            try {
                return context.invokeFunction(name, args);
            }
            catch (final Throwable t) {
                throw new EngineException(this, t);
            }
        }
    }
}
