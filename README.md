# Graal.js engine

<!-- badges: start -->
[![license: GPL v3](https://img.shields.io/badge/License-GPLv3-blue)](https://www.gnu.org/licenses/gpl-3.0)
[![Twitter](https://img.shields.io/twitter/follow/Icy_BioImaging)](https://twitter.com/Icy_BioImaging)
[![Image.sc forum](https://img.shields.io/badge/discourse-forum-brightgreen)](https://forum.image.sc/tag/icy)
[![Icy version](https://img.shields.io/badge/icy-v3.0.0-D4E2FF)](https://icy.bioimageanalysis.org)
[![Plugin version](https://img.shields.io/badge/plugin_library-v1.0.0-D4E2FF)](https://gitlab.pasteur.fr/bia/graal-for-icy/-/tags)
<!-- badges: end -->

This is the repository for the source code of *Graal.js engine for Icy*, a plugin for the [bioimage analysis software Icy](https://icy.bioimageanalysis.org/), which was developed by members or former members of the [Biological Image Analysis unit at Institut Pasteur](https://research.pasteur.fr/en/team/bioimage-analysis/). This plugin is licensed under GPL3 license.     
Icy is developed and maintained by [Biological Image Analysis unit at Institut Pasteur](https://research.pasteur.fr/en/team/bioimage-analysis/). The [source code of Icy](https://gitlab.pasteur.fr/bia/icy) is also licensed under a GPL3 license.


## Plugin description

<!-- Short description of package goals, with descriptive links to the documentation website --> 
Graal.js Script Engine for Icy.



## Installation instructions

For end-users, refer to the documentation on the Icy website on [how to install an Icy plugin](https://icy.bioimageanalysis.org/tutorial/how-to-install-an-icy-plugin/).

For developers, see our [Contributing guidelines](https://gitlab.pasteur.fr/bia/icy/-/blob/master/CONTRIBUTING.md) and [Code of Conduct](https://gitlab.pasteur.fr/bia/icy/-/blob/master/CODE-OF-CONDUCT.md).

<!--  Here we should have some explanations on how to fork this repo (for an example see https://gitlab.pasteur.fr/bia/wellPlateReader). Add any info related to Maven etc. How the project is build (for an example see https://gitlab.pasteur.fr/bia/wellPlateReader). Any additional setup required (authentication tokens, etc).  -->


## Main functions and usage

<!-- List main functions, explain architecture, classname, give info on how to get started with the plugin. If applicable, how the package compares to other similar packages and/or how it relates to other packages -->

Classname: `plugins.tmusset.library.GraalJSPlugin`



## Citation

Please cite this plugin as follows:


Please also cite the Icy software and mention the version of Icy you used (bottom right corner of the GUI or first lines of the Output tab):     
de Chaumont, F. et al. (2012) Icy: an open bioimage informatics platform for extended reproducible research, [Nature Methods](https://www.nature.com/articles/nmeth.2075), 9, pp. 690-696       
https://icy.bioimageanalysis.org



## Author(s)

Thomas MUSSET


## Additional information

